//
//  AppDelegate.swift
//  Font Explorer
//
//  Created by William Dillon on 12/20/14.
//  Copyright (c) 2014 HouseDillon. All rights reserved.
//

import Cocoa

let initialSizes = [6, 7, 8, 9, 10, 11, 12, 14, 18, 21, 24, 36, 48, 60, 72, 144]

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSTextFieldDelegate, NSWindowDelegate {

    @IBOutlet weak var window:       NSWindow!
    @IBOutlet weak var familiesSelector: NSComboBox!
    @IBOutlet weak var sizeSelector: NSComboBox!
    @IBOutlet weak var textField:    NSTextField!
    @IBOutlet weak var fgColor:      NSColorWell!
    @IBOutlet weak var bgColor:      NSColorWell!
    @IBOutlet weak var italics:      NSButton!
    @IBOutlet weak var bold:         NSButton!
    @IBOutlet weak var underline:    NSButton!
    @IBOutlet var textView: NSTextView!
    @IBOutlet weak var autoUpdate: NSButton!

    var fontFamilies = [String]()
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {

        // Load the font list into the selector and select all fonts by default
        fontFamilies = NSFontManager.sharedFontManager().availableFontFamilies as [String]
        familiesSelector.addItemWithObjectValue("All Fonts")
        familiesSelector.addItemsWithObjectValues(fontFamilies)
        familiesSelector.selectItemAtIndex(0)
        
        // Add the typical font sizes and select 12pt font
        sizeSelector.addItemsWithObjectValues(initialSizes)
        sizeSelector.selectItemAtIndex(6)

        textView.editable = false
        
        // Perform all the update actions to make sure there's a display
        self.update(self)
    }

    override func controlTextDidChange(obj: NSNotification) {
        if autoUpdate.state == NSOnState {
            self.update(self)
        }
    }
    
    @IBAction func parameterChanged(sender: AnyObject?) {
        if autoUpdate.state == NSOffState {
            return
        }

        update(sender)
    }
    
    @IBAction func update(selector: AnyObject?) {
        
        // Remove all existing text from the textView
        textView.textStorage!.replaceCharactersInRange(NSRange(location: 0, length: textView.textStorage!.length),
            withString: "")
        
        // Prepare a list of fonts.  For now this is either all of them or one
        var fontList = Array<NSFont?>()
        if familiesSelector.stringValue == "All Fonts" {
            let fontSize = sizeSelector.floatValue
            fontList = fontFamilies.map({ (input: String) -> NSFont? in
//                var font = NSFont(name: input, size: CGFloat(fontSize))
                var fontTraits = NSFontTraitMask()
                if self.bold.state == NSOnState {
                    fontTraits |= NSFontTraitMask.BoldFontMask
                }
                if self.italics.state == NSOnState {
                    fontTraits |= NSFontTraitMask.ItalicFontMask
                }

                var font = NSFontManager.sharedFontManager().fontWithFamily(input, traits: fontTraits, weight: 0, size: CGFloat(fontSize))
                return font
            })
            
        } else {
            let fontSize = sizeSelector.floatValue
            var font = NSFont(name: familiesSelector.stringValue, size: CGFloat(fontSize))
            fontList.append(font)
        }
        
        // Iterate through the fonts and create an attributed string
        // containing the sample text in each font.
        var attributes = [NSObject : AnyObject]()
        var attrString = NSTextStorage()
        attributes[NSForegroundColorAttributeName] = fgColor.color
        attributes[NSBackgroundColorAttributeName] = bgColor.color
        if underline.state == NSOnState {
            attributes[NSUnderlineStyleAttributeName] = NSUnderlineStyleSingle
        }
        for font in fontList {
            // Explicitly check whether the font is non-nil
            if font != nil {
                attributes[NSToolTipAttributeName] = font!.displayName
                attributes[NSFontAttributeName] = font
                textView.textStorage?.appendAttributedString(NSAttributedString(string: "\(textField.stringValue)\t(\(font!.displayName!))\n",
                    attributes: attributes))
            }
        }
    }
    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

