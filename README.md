FontScape
=============

Do you want to see how a sentence would look in each installed font? Use FontScape!

There really isn't too much to it.  Type your sentence in the text field in the bottom of the window and click 'update.'  If you want your changes to be visible immediately, turn on auto updates.
